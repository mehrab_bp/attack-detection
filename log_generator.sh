#!/bin/bash

LOG_FILE="/usr/share/nginx/html/logs/access.log"
TIMESTAMP=$(date +"%Y-%m-%d %H:%M:%S")

LOG_ENTRY0="{ \"timestamp\": \"$TIMESTAMP\", \"message\": \"Log entry generated at $TIMESTAMP\" }"
LOG_ENTRY1="{ \"timestamp\": \"$TIMESTAMP\", \"message\": \"Something clicked at $TIMESTAMP\" }"
LOG_ENTRY2="{ \"timestamp\": \"$TIMESTAMP\", \"message\": \"user defined something\" }"
LOG_ENTRY3="{ \"timestamp\": \"$TIMESTAMP\", \"message\": \"erorr in finding user\" }"
LOG_ENTRY4="{ \"timestamp\": \"$TIMESTAMP\", \"message\": \"load balancer warning at $TIMESTAMP\" }"
LOG_ENTRY5="{ \"timestamp\": \"$TIMESTAMP\", \"message\": \"action is running!\" }"
LOG_ENTRY6="{ \"timestamp\": \"$TIMESTAMP\", \"message\": \"user failed to complete action\" }"
LOG_ENTRY7="{ \"timestamp\": \"$TIMESTAMP\", \"message\": \"retrying actions for user with id = 12345\" }"
LOG_ENTRY8="{ \"timestamp\": \"$TIMESTAMP\", \"message\": \"action is running!\" }"
LOG_ENTRY9="{ \"timestamp\": \"$TIMESTAMP\", \"message\": \"action completed at $TIMESTAMP\" }"


# Append the JSON log entry to the log file
echo "$LOG_ENTRY0" >> "$LOG_FILE"
echo "$LOG_ENTRY1" >> "$LOG_FILE"
echo "$LOG_ENTRY2" >> "$LOG_FILE"
echo "$LOG_ENTRY3" >> "$LOG_FILE"
echo "$LOG_ENTRY4" >> "$LOG_FILE"
echo "$LOG_ENTRY5" >> "$LOG_FILE"
echo "$LOG_ENTRY6" >> "$LOG_FILE"
echo "$LOG_ENTRY7" >> "$LOG_FILE"
echo "$LOG_ENTRY8" >> "$LOG_FILE"
echo "$LOG_ENTRY9" >> "$LOG_FILE"

