# Use an existing Docker image as a base
FROM nginx:latest

# Copy your application files into the Docker image
COPY . /usr/share/nginx/html

# Install cron and other necessary tools
RUN apt-get update && apt-get install -y cron

# Add a script to generate logs
COPY ./log_generator.sh /log_generator.sh
RUN chmod +x /log_generator.sh

# Add the cron job to schedule log generation
RUN echo "* * * * * root /log_generator.sh >> /var/log/cron.log 2>&1" > /etc/cron.d/log-generator
RUN chmod 0644 /etc/cron.d/log-generator
RUN crontab /etc/cron.d/log-generator

# Expose the port the web server will listen on
EXPOSE 80
